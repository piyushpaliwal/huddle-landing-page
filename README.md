# Frontend Mentor - Huddle landing page with single introductory section solution

This is a solution to the [Huddle landing page with single introductory section challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/huddle-landing-page-with-a-single-introductory-section-B_2Wvxgi0). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

**Note: Delete this note and update the table of contents based on what sections you keep.**

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the page depending on their device's screen size
- See hover states for all interactive elements on the page

### Screenshot

![Desktop](./screenshot-desktop.png)
![Mobile](./screenshot-mobile.png)

### Links

- Solution URL: [https://gitlab.com/piyushpaliwal/huddle-landing-page](https://gitlab.com/piyushpaliwal/huddle-landing-page)
- Live Site URL: [https://melodious-marigold-615aa3.netlify.app](https://melodious-marigold-615aa3.netlify.app)

## My process

### Built with

- Vite
- CSS Grid
- Desktop-first workflow
- [React](https://reactjs.org/) - JS library
- Tailwind CSS
- Prettier

### What I learned

- Responsive background images
- Deeper understanding of Tailwind CSS
- How can we apply styles for body using `useEffect` hook

### Continued development

- Although body has padding defined, the footer element ignores this padding when resizing to a very small screen. I don't have a solution to this at the moment.

## Author

- Website - [Piyush Paliwal](https://ipiyush.me)
- Frontend Mentor - [@piyushpaliwal](https://www.frontendmentor.io/profile/piyushpaliwal)
- Twitter - [@\_ppaliwal](https://www.twitter.com/_ppaliwal)
