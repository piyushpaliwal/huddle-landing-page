import './App.css'
import logo from './assets/images/logo.svg'
import illustration from './assets/images/illustration-mockups.svg'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faFacebookF,
  faTwitter,
  faInstagram,
} from '@fortawesome/free-brands-svg-icons'
import { useEffect } from 'react'

function App() {
  useEffect(() => {
    document.body.classList.add(
      'bg-mobile',
      'md:bg-desktop',
      'text-white',
      'bg-cover',
      'bg-no-repeat',
      'bg-primary-violet'
    )
  }, [])
  return (
    <>
      <div className="grid grid-flow-row content-between h-screen gap-10 p-10">
        <header>
          <nav>
            <img src={logo} alt="logo" className="h-10" />
          </nav>
        </header>
        <main className="grid grid-flow-row md:grid-flow-col md:grid-cols-2 gap-10 place-items-center">
          <img src={illustration} alt="illustration mockups" className="" />
          <div className="grid grid-flow-row gap-5 md:justify-around md:justify-items-start justify-items-center text-center md:text-left">
            <h1 className="text-3xl md:text-4xl font-semibold leading-12">
              Build The Community Your Fans Will Love
            </h1>
            <p className="text-white/70">
              Huddle re-imagines the way we build communities. You have a voice,
              but so does your audience. Create connections with your users as
              you engage in genuine discussion.
            </p>
            <button className="bg-white rounded-full text-primary-violet px-12 py-3 font-semibold text-sm shadow-xl hover:bg-primary-magenta hover:text-white">
              Register
            </button>
          </div>
        </main>
        <footer className="grid grid-flow-col md:justify-end justify-center gap-5">
          <FontAwesomeIcon
            icon={faFacebookF}
            className="rounded-full aspect-square border p-3 border-white bg-transparent hover:text-primary-magenta hover:border-primary-magenta"
          />
          <FontAwesomeIcon
            icon={faTwitter}
            className="rounded-full aspect-square border p-3 border-white bg-transparent hover:text-primary-magenta hover:border-primary-magenta"
          />
          <FontAwesomeIcon
            icon={faInstagram}
            className="rounded-full aspect-square border p-3 border-white bg-transparent hover:text-primary-magenta hover:border-primary-magenta"
          />
        </footer>
      </div>
    </>
  )
}

export default App
