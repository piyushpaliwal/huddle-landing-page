/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')
export default {
  content: ['./src/**/*.{html,js,jsx}'],
  theme: {
    extend: {
      colors: {
        'primary-violet': 'hsl(257, 40%, 49%)',
        'primary-magenta': 'hsl(300, 69%, 71%)',
      },
      backgroundImage: {
        mobile: "url('../public/images/bg-mobile.svg')",
        desktop: "url('../public/images/bg-desktop.svg')",
      },
      fontFamily: {
        sans: ['Open Sans', ...defaultTheme.fontFamily.sans],
      },
      lineHeight: {
        12: '3rem',
      },
    },
  },
  plugins: [],
}
